﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] Transform m_WeaponPivot;
    [SerializeField] Transform m_WeaponNuzzle;
    [SerializeField] Transform m_GroundCheckTransform;
    [SerializeField] GameObject m_Bullet;
    [SerializeField] float m_Speed;
    [SerializeField] float m_WalkingSpeed;
    [SerializeField] float m_JumpForce;
    Rigidbody2D mRigidbody;

    void Start()
    {
        mRigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Aim();
        Move();
        Fire();
    }

    void Aim()
    {
        Vector3 transformedPosition = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float nuzzleAngle = Mathf.Atan2(transformedPosition.y, transformedPosition.x) * Mathf.Rad2Deg;
        m_WeaponPivot.transform.rotation = Quaternion.Euler(Vector3.forward * nuzzleAngle);
    }

    void Move()
    {
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        float speedModifier = Input.GetButton("Walk") ? m_WalkingSpeed : m_Speed;
        Vector2 positionOffset = (Vector2.up * mRigidbody.velocity) + (Vector2.right * horizontalMovement * speedModifier);
        print(positionOffset);
        mRigidbody.velocity = positionOffset;
        
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            mRigidbody.AddForce(Vector3.up * m_JumpForce);
        }
    }

    bool IsGrounded()
    {
        return Physics2D.Linecast(transform.position, m_GroundCheckTransform.position, 1 << LayerMask.NameToLayer("Ground"));
    }

    void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject bullet = Instantiate(m_Bullet, m_WeaponNuzzle.position, m_WeaponPivot.rotation);
            bullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * 1000.0f);
        }
    }
}
